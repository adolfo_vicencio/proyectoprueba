<?php 
include('connection.php');
include('validador.php');
include('log_eventos_func.php');
session_start();
//definición de variables de validación
$correo=null;
$numero=null;
$mensaje=null;
$RutValidado= null;
$PasswordsValidados=null;

if (isset($_POST['register'])) {
 
$rut_usuario      = $_POST['rut_usuario'];
$nombre_usuario   = $_POST['nombre_usuario'];
$apellidos_usuario= $_POST['apellidos_usuario'];
$email_usuario    = $_POST['email_usuario'];
$id_perfil        = $_POST['id_perfil'];
$clave_usuario    = $_POST['clave_usuario'];
$clave_val        = $_POST['clave_val'];
//validaciones

//ValidaRUT
if (valida_rut($rut_usuario)){
    //echo "rut ok";
    $RutValidado=TRUE;
    
}else{
    //echo "rut no ok";
    $RutValidado=FALSE;
}
//Valida Passwords
if (ValidaPasswords($clave_usuario, $clave_val)){
    $PasswordsValidados=true;
    //echo "passwords iguales";
}else{
    $PasswordsValidados=false;
    //echo "passwords distintos";
}
//bdd
$connection= ConectarBdd();
// valido que el correo no fuera asignado con anterioridad.
$consulta = "SELECT * FROM usuarios WHERE email_usuario='$email_usuario'";
$result = $connection->query($consulta);
$correo = $result->num_rows;


 // acá valido las variables importantes de la base de datos
    if ($correo == 0 && $RutValidado==TRUE && $PasswordsValidados==true) {
        $rut_usuario= str_replace ("-","",$rut_usuario );
// valido que el rut ingresado no exista.
//		echo "pase las validaciones";
		$consulta = "SELECT * FROM usuarios WHERE rut_usuario='$rut_usuario'";
		$result = $connection->query($consulta);
		$numero = $result-> num_rows;
		if ($numero > 0) {
//			echo "error el registro ya existe";
		}else {
			$sql = "INSERT INTO usuarios (rut_usuario, nombre_usuario, apellidos_usuario, email_usuario, clave_usuario, id_perfil) VALUES ('$rut_usuario','$nombre_usuario','$apellidos_usuario','$email_usuario','$clave_usuario','$id_perfil')";
			if (mysqli_query($connection, $sql)) {
				$mensaje = 1;
				$_POST['rut_usuario'] = null;
				$_POST['nombre_usuario']= null;
				$_POST['apellidos_usuario']= null;
				$_POST['email_usuario']= null;
				$_POST['id_perfil']= null;
				$_POST['clave_usuario']= null;
				$_POST['clave_val']= null;
				 mysqli_close($connection);
                                $registro_log= RegistraEvento("CREAR", "USUARIOS", "Usuario: $rut_usuario-$nombre_usuario-$apellidos_usuario - $email_usuario - $id_perfil", $_SESSION["rut"]);
			} else {
				echo "Error: " . $sql . "<br>" . mysqli_error($connection);
				}
		}
//        header( "refresh:1;url=iniciosesion.php" );
    }else{
        //echo "Errores de validación"; 
    }
}
 
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Electrónica</title>
    <!-- Bootstrap -->
    <link href="css/bootstrap-4.4.1.css" rel="stylesheet">
</head>
<body>  
    <hr>
		<h5 class="text-center">Complete el formulario para la creación de nuevos usuarios </h5>
    <hr>
	<form method="post" action="" name="singnup-form">
    <div class="container">
		<div class="long_row text-center"> 
			<div class="card">
				
				<div class="card-body">
					<!-- <h5 class="card-title">Complete el formulario para la creación de nuevos usuarios </h5> -->
					
					<div class="row text-left"> 
						<table align="center">	
							<tr>
								<td width="190">
									<label for = "rut"  > Ingrese Rut   </label>
								:</td>
								<td width="252">
									<input name="rut_usuario" type= "text" autofocus="autofocus" required="" id="rut" value="<?php if (isset($_POST['rut_usuario'])){ echo $_POST['rut_usuario']; } ?>" size="40" maxlength="10" oninput="checkRut(this)">
									<?php
									if (!$RutValidado && ! is_null($RutValidado ) ){
										echo "<td width='250'> <label >**Rut inválido</label></td>";
									}else {
										if ($numero > 0 ){
											echo "<td width='250'> <label >**Error el Rut ya existe.</label></td>";
											}
										}
									?>
								</td>
							</tr>
							<tr>
								<td>
									<label for = "nombre"  > Ingrese Nombre :</label>	
								</td>
								<td>
									<input type= "text"  size="40" maxlength="40" name="nombre_usuario" id="nombre" required="" value="<?php if (isset($_POST['nombre_usuario'])){ echo $_POST['nombre_usuario']; } ?>">
								</td>
							</tr>
							<tr>
								<td>
									<label for = "apellidos"  > Ingrese Apellidos :</label>
								</td>
								<td>
									<input type= "text" size="40" maxlength="40" name="apellidos_usuario" id="apellidos" required="" value="<?php if (isset($_POST['apellidos_usuario'])){ echo $_POST['apellidos_usuario']; } ?>">
								</td>
	
							</tr>
							<tr>
								<td>
									<label for = "email_ing"  > Ingrese eMail :</label>
								</td>
								<td>
									<input type= "email"   placeholder="correo@correo.cl"    size="40" maxlength="30" name="email_usuario" id="mail_ing" required="" value="<?php if (isset($_POST['email_usuario'])){ echo $_POST['email_usuario']; } ?>"> 
									<?php
									if ($correo > 0 ){
										echo "<td width='250'> <label >**Correo registrado ya existe</label></td>";
									}
									?>
								</td>
	
							</tr>
							<tr>
								<td>
									<label for = "perfil"  > Ingrese Perfil :</label>
								</td>
								<td>
									<input type= "text" size="40" maxlength="4" name="id_perfil" id="perfil" required="" value="<?php if (isset($_POST['id_perfil'])){ echo $_POST['id_perfil']; } ?>">
								</td>
	
							</tr>
							<tr>
								<td>
									<label for = "clave_ing" > Ingrese Contraseña :</label>
								</td>
								<td>
									<input type= "password" placeholder="Ingresar contraseña" size="40" maxlength="12" name="clave_usuario" id="clave_ing" required="" value=<?php if (isset($_POST['clave_usuario'])){ echo $_POST['clave_usuario']; } ?>>
								</td>
	
							</tr>
							<tr>
								<td>
									<label for = "clave_val"  > Confirme Contraseña :</label>
								</td>
								<td>
									<input type= "password" placeholder="Confirmar contraseña" size="40" maxlength="12" name="clave_val" id="clave_val" required="" value=<?php if (isset($_POST['clave_val'])){ echo $_POST['clave_val']; } ?>>
									<?php
									if (!$PasswordsValidados && ! is_null($PasswordsValidados ) ){
										echo "<td width='293'> <label >**Password no coinciden, reingresar</label></td>";
									}
									?>
								</td>
	
							</tr>
							<tr>
								<td>
									<?php 
										if ($mensaje ==1 ) {
											echo "<td width='293'> <label > <b> ***Se creado el registro correctamente *** </b></label></td>";
										}
									?>						
								</td>
								<td>
								</br></td>
	
							</tr>
						</table>
						<table width="643" border="0" align="center">
						<tbody>
							<tr>
								<td width="637" align="right">
									<button type="submit" name="register" value="register" href="#" class="btn btn-primary"> Agregar Usuario </button>
								</td>
							</tr>	 
						</tbody>
						</table>
					</div>
					<table width="800" border="0" align="center">
						<tbody>
							<tr>
								<td width="680" align="right">&nbsp;
								</td>
								<td width="120" align="right">
									<h6 class="card-title"> <a class="nav-link" href="inicio.php" target="principal" > ir al home </a> </h6>
								</td>
							</tr>	 
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	</form>
    <div class="container text-white bg-dark p-4">
		<div class="row">
			<div class="col-6 col-md-8 col-lg-7">
			</div> 
			<div class="col-md-4 col-lg-5 col-6">
				<address>
					<strong> CASA MATRIZ SANTIAGO, Inc.</strong><br>
					Avenida Principal #1236<br>
					Providencia, Santiago de Chile<br>
					<abbr title="Phone">Teléfono</abbr> (+56)2.2422.5500
				</address>
				<address>
					<strong>Correo Electrónico</strong><br>
					<a href="mailto:#">contacto@electronica.cl</a>
				</address>
			</div>
		</div>
    </div>
    <footer class="text-center">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<p>Copyright © Adolfo. Todos los derechos reservados.</p>
				</div>
			</div>
		</div>
    </footer>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery-3.4.1.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap-4.4.1.js"></script>
    <script src="js/validador.js"></script>
 </body>
</html>
